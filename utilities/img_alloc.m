
function img = img_alloc(in)
% Allocates image structure. 

if ischar(in)
    in = imread(in);
end

if ndims(in) == 3
    img.origin = in;
    img.input = rgb2gray(in);
else
    img.origin = cat(3,in,in,in);
    img.input = in;
end

img.blur = img_blur(img.input,1.5);

