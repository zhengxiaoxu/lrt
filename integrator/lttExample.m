
function [positions,target_sz,time,num_detect] = lttExample(opt,video_path,img_files,show_visualization)

% =========================================================================
% Initialization
% =========================================================================
% initialization on the first frame based on groundtruth 
opt.source.im  = img_alloc([video_path img_files{1}]);
ltt = lttInit(opt,[]);
positions(1,:) = ltt.pos;
target_sz(1,:) = ltt.target_sz;

% visualization
if (show_visualization)
    ltt = lttDisplay(0,ltt,1,numel(img_files)); 
end


% =========================================================================
% Running tracker
% =========================================================================

% intialize variables for calculating FPS and distance precision
detect_flag = 0;
time = 0;

for frame = 2:numel(img_files) 
    
    ltt.img{frame} = img_alloc([video_path img_files{frame}]); 

    tic();
    [ltt,detect_flag,num_detect] = lttProcessFrame(ltt,frame,detect_flag); 
    time = time + toc();
    positions(frame,:) = ltt.pos;
    target_sz(frame,:) = ltt.target_sz;
    
    % visualization
    if (show_visualization)
        lttDisplay(1,ltt,frame,numel(img_files));
    end
   
end

% bb = ltt.bb';
end