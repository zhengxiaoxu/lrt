
function [ltt,detect_flag,num_detect] = lttProcessFrame(ltt,frame,detect_flag)

% =========================================================================
% Pure tracking
% =========================================================================
if ltt.control.update_detector == 0
    [ltt,tBB,tConf] = lttTracking(ltt,frame); % frame-to-frame tracking
    
    ltt.valid(frame) = 2;
    ltt.bb(:,frame)  = tBB;
    ltt.conf(frame)  = tConf;
    
    ltt = lttUpdateTracker(ltt,frame);
end


% =========================================================================
% Tracking and detection
% =========================================================================
num_detect = 0;
if ltt.control.update_detector == 1
    [ltt,tBB,tConf] = lttTracking(ltt,frame); % frame-to-frame tracking
    if isempty(tConf)
        tConf = 0;
    end
    
    if (tConf > ltt.switchpara.thr_tracker && detect_flag == 0) || (tConf > ltt.switchpara.thr_switch && detect_flag == 1)
        ltt.valid(frame) = 2;
        ltt.bb(:,frame)  = tBB;
        ltt.conf(frame)  = tConf;
        detect_flag = 0;
        
    else
%      if tConf < ltt.switchpara.thr_tracker
        [ltt,dBB,dConf] = lttDetection(ltt,ltt.img{frame});
        if isempty(dConf)
            dConf = 0;
        end
        if dConf > ltt.switchpara.thr_detector
            ltt.valid(frame) = 1;
%             ltt.bb(:,i) = floor(dBB(:,1));
%             ltt.bb(4,i) = floor(ltt.bb(2,i)+ltt.base_target_sz(1)/ltt.base_target_sz(2)*(ltt.bb(3,i)-ltt.bb(1,i)));
%             ltt.conf(i) = dConf;
            
            ltt.pos = floor([(dBB(2)+dBB(4))/2,(dBB(1)+dBB(3))/2]);
            ltt.currentScaleFactor = sqrt((dBB(4)-dBB(2))*(dBB(3)-dBB(1))/(ltt.base_target_sz(2)*ltt.base_target_sz(1)));
            
            num_detect = num_detect + 1;
            detect_flag = 1;
        else
            ltt.valid(frame) = 0;
            detect_flag = 1;
        end
    end
    
    if tConf > ltt.switchpara.thr_update
        ltt = lttUpdateTracker(ltt,frame);
        ltt = lttUpdateDetector(ltt,frame);
    end
end