
function ltt = lttDisplay(varargin)

if varargin{1}==0
    ltt = varargin{2};
    
    if ~isfield(ltt,'handle')
        ltt.handle = imshow(ltt.img{1}.origin,'initialmagnification','fit');
    end
    
    % Draw image
    img = ltt.img{1}.origin;
    set(ltt.handle,'cdata',img);
    hold on;
    
    h = get(gca,'Children');
    if length(h) > 1
        delete(h(2:end));
    end
    
    % Draw Box
    bb_draw(ltt.bb(:,1));
    
    % Info
    string = ['#' num2str(1) '/' num2str(varargin{end})];
    text(10,10,string,'color','white','backgroundcolor','k');
     
else
    ltt = varargin{2};
    i = ltt.source.idx(varargin{3});
    
    h = get(gca,'Children');
    delete(h(1:end-1));

    % Draw image
    img = ltt.img{i}.origin;
    set(ltt.handle,'cdata',img); 
    hold on;

    % Draw Track
    if ltt.valid(i) == 2 
        color = 'y';
        linewidth = 2;
        bb_draw([ltt.bb(:,i);ltt.conf(i)],'linewidth',linewidth,'edgecolor',color,'curvature',[0 0]);
    end
    
    if ltt.valid(i) == 1 
        color = 'r';
        linewidth = 2;
        bb_draw([ltt.bb(:,i);ltt.conf(i)],'linewidth',linewidth,'edgecolor',color,'curvature',[0 0]);
    end

    % Info
    string = ['#' num2str(i) '/' num2str(varargin{end})];
    text(5,15,string,'color','white','backgroundcolor','k');

    drawnow;
    
end