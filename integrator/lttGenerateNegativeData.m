
function [nX,nEx] = lttGenerateNegativeData(ltt,overlap,img)

% Measure patterns on all bboxes that are far from initial bbox

idxN        = find(overlap > ltt.n_par.overlap_min & overlap < ltt.n_par.overlap_max);

if length(idxN)>ltt.n_par.num_patterns
    idxN        = idxN(1:ltt.n_par.num_patterns);
end
[nX,status] = fern(5,img,idxN,ltt.var/2);


% idxN        = idxN(status==1); % bboxes far and with big variance
% nX          = nX(:,status==1);


% Randomly select 'num_patches' bboxes and measure patches
idx = randvalues(1:length(idxN),ltt.n_par.num_patches);
bb  = ltt.grid(:,idxN(idx));
nEx = lttGetPattern(img,bb,ltt.model.patchsize);