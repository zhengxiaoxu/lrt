
function ltt = lttUpdateDetector(ltt,frame)

bb    = ltt.bb(:,frame); % current bounding box
img   = ltt.img{frame}; % current image

% generate positive data
overlap  = bb_overlap(bb,ltt.grid); % measure overlap of the current bounding box with the bounding boxes on the grid
[pX,pEx] = lttGeneratePositiveData(ltt,overlap,img,ltt.p_par_update); % generate positive examples from all bounding boxes that are highly overlappipng with current bounding box
pY       = ones(1,size(pX,2)); % labels of the positive patches

% generate negative data
[nX,nEx] = lttGenerateNegativeData(ltt,overlap,img);
nY       = zeros(1,size(nX,2));


fern(2,[pX nX],[pY nY],ltt.model.thr_fern,2); % update the Ensemble Classifier (reuses the computation made by detector)

ltt = lttTrainNN(pEx,nEx,ltt); % update nearest neighbour 

