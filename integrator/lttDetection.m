
function [ltt,dBB,dConf] = lttDetection(ltt,img)
% scanns the image(I) with a sliding window, returns a list of bounding
% boxes and their confidences that match the object description

dBB        = [];
dConf      = [];
dt        = struct('bb',[],'idx',[],'conf1',[],'conf2',[],'isin',nan(3,1),'patt',[],'patch',[]);

fern(4,img,ltt.control.maxbbox,ltt.var,ltt.tmp.conf,ltt.tmp.patt); % evaluates Ensemble Classifier: saves sum of posteriors to 'ltt.tmp.conf', saves measured codes to 'ltt.tmp.patt', does not considers patches with variance < tmd.var
idx_dt = find(ltt.tmp.conf > ltt.model.num_trees*ltt.model.thr_fern); % get indexes of bounding boxes that passed throu the Ensemble Classifier

if length(idx_dt) > 100 % speedup: if there are more than 100 detections, pict 100 of the most confident only
    [~,sIdx] = sort(ltt.tmp.conf(idx_dt),'descend');
    idx_dt = idx_dt(sIdx(1:100));
end

num_dt = length(idx_dt); % get the number detected bounding boxes so-far 
if num_dt == 0  % if nothing detected, return
    return; 
end 


% BB = ltt.grid(1:4,idx_dt);
% Conf = ltt.tmp.conf(idx_dt)/ltt.model.num_trees;
% 
% [~,index] = max(Conf);
% dBB = BB(:,index);
% dConf = Conf(index);



% initialize detection structure
dt.bb     = ltt.grid(1:4,idx_dt); % bounding boxes
dt.patt   = ltt.tmp.patt(:,idx_dt); % corresponding codes of the Ensemble Classifier
dt.idx    = find(idx_dt); % indexes of detected bounding boxes within the scanning grid
dt.conf1  = nan(1,num_dt); % Relative Similarity (for final nearest neighbour classifier)
dt.conf2  = nan(1,num_dt); % Conservative Similarity (for integration with tracker)
dt.isin   = nan(3,num_dt); % detected (isin=1) or rejected (isin=0) by nearest neighbour classifier
dt.patch  = nan(prod(ltt.model.patchsize),num_dt); % Corresopnding patches

for i = 1:num_dt % for every remaining detection
    ex   = lttGetPattern(img,dt.bb(:,i),ltt.model.patchsize); % measure patch
    [conf1, conf2, isin] = lttNN(ex,ltt); % evaluate nearest neighbour classifier
    
    % fill detection structure
    dt.conf1(i)   = conf1;
    dt.conf2(i)   = conf2;
    dt.isin(:,i)  = isin;
    dt.patch(:,i) = ex;
    
end
idx = dt.conf1 > ltt.model.thr_nn; % get all indexes that made it through the nearest neighbour


% output
BB    = dt.bb(:,idx); % bounding boxes
Conf  = dt.conf2(:,idx); % conservative confidences

[cBB,cConf,cSize] = bb_cluster_confidence(BB,Conf); % cluster detections

if sum(idx) ~= 0
    [~,index] = max(cConf);
    dBB = cBB(:,index);
    dConf = cConf(index);
end





