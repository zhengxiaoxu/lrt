
function ltt = lttUpdateTracker(ltt,frame)

% load parameters from ltt structure
% target location
pos = ltt.pos;

% tracker parameters	
interp_factor = ltt.trackerpara.interp_factor;	
num_compressed_dim = ltt.trackerpara.num_compressed_dim;
nScales = ltt.trackerpara.number_of_scales;
% load image
im = ltt.img{frame}.origin;

% compute coefficients for the tranlsation filter
[xl_npca, xl_pca] = get_subwindow(im, pos, ltt.window_sz, ltt.currentScaleFactor);

ltt.h_num_pca = (1 - interp_factor) * ltt.h_num_pca + interp_factor * xl_pca;
ltt.h_num_npca = (1 - interp_factor) * ltt.h_num_npca + interp_factor * xl_npca;

data_matrix = ltt.h_num_pca;

[pca_basis, ~, ~] = svd(data_matrix' * data_matrix);
ltt.projection_matrix = pca_basis(:, 1:num_compressed_dim);

hf_proj = fft2(feature_projection(ltt.h_num_npca, ltt.h_num_pca, ltt.projection_matrix, ltt.cos_window));
ltt.hf_num = bsxfun(@times, ltt.yf, conj(hf_proj));

xlf = fft2(feature_projection(xl_npca, xl_pca, ltt.projection_matrix, ltt.cos_window));
new_hf_den = sum(xlf .* conj(xlf), 3);

ltt.hf_den = (1 - interp_factor) * ltt.hf_den + interp_factor * new_hf_den;

%Compute coefficents for the scale filter
if nScales > 0

    %create a new feature projection matrix
    [xs_pca, xs_npca] = get_scale_subwindow(im, pos, ltt.base_target_sz, ltt.currentScaleFactor*ltt.scaleSizeFactors, ltt.scale_model_sz);
    ltt.s_num = (1 - interp_factor) * ltt.s_num + interp_factor * xs_pca;

    bigY = ltt.s_num;
    bigY_den = xs_pca;

    if ltt.max_scale_dim
        [ltt.scale_basis, ~] = qr(bigY, 0);
        [ltt.scale_basis_den, ~] = qr(bigY_den, 0);
    else
        [U,~,~] = svd(bigY,'econ');
        ltt.scale_basis = U(:,1:ltt.s_num_compressed_dim);
    end
    ltt.scale_basis = ltt.scale_basis';

    %create the filter update coefficients
    sf_proj = fft(feature_projection_scale([],ltt.s_num,ltt.scale_basis,ltt.scale_window),[],2);
    ltt.sf_num = bsxfun(@times,ltt.ysf,conj(sf_proj));

    xs = feature_projection_scale(xs_npca,xs_pca,ltt.scale_basis_den',ltt.scale_window);
    xsf = fft(xs,[],2);
    new_sf_den = sum(xsf .* conj(xsf),1);
    ltt.sf_den = (1 - interp_factor) * ltt.sf_den + interp_factor * new_sf_den;
end
end





