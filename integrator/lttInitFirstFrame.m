
function BB = lttInitFirstFrame(source,frame,min_win)
% set the initial bounding box: 

while 1
    bb = [p(1);p(2);p(1)+p(3);p(2)+p(4)];
    source.im0  = img_get(source,frame);
    
    % check
    if ~isempty(bb) && min(bb_size(bb)) >= min_win
        BB = bb;
        break;
    end   
end
