
function ltt = lttTrainNN(pEx,nEx,ltt)

nP = size(pEx,2); % get the number of positive example 
nN = size(nEx,2); % get the number of negative examples

x = [pEx,nEx];
y = [ones(1,nP), zeros(1,nN)];

% Permutate the order of examples
idx = randperm(nP+nN); % 
if ~isempty(pEx)
    x   = [pEx(:,1) x(:,idx)]; % always add the first positive patch as the first (important in initialization)
    y   = [1 y(:,idx)];
end
   
for k = 1:1 % Bootstrap
   for i = 1:length(y)
       
       [conf1,~,isin] = lttNN(x(:,i),ltt); % measure Relative similarity
       
       % Positive
       if y(i) == 1 && conf1 <= ltt.model.thr_nn % 0.65
           if isnan(isin(2))
               ltt.pex = x(:,i);
               continue;
           end
            %if isin(2) == size(ltt.pex,2)
              %ltt.pex = [ltt.pex x(:,i)]; 
            %else
            ltt.pex = [ltt.pex(:,1:isin(2)) x(:,i) ltt.pex(:,isin(2)+1:end)]; % add to model
            %end
       end
       
       % Negative
       if y(i) == 0 && conf1 > 0.5
           ltt.nex = [ltt.nex x(:,i)];
       end
   end
end