
function [pX,pEx,bbP0] = lttGeneratePositiveData(ltt,overlap,im0,p_par)

pX   = [];
pEx  = [];%zeros(prod(ltt.patchsize),numWarps);

% Get closest bbox
[~,idxP] = max(overlap);
if isempty(idxP)
    exit('The initial bounding box is too small!')
end
bbP0 =  ltt.grid(1:4,idxP);

% Get overlapping bboxes
idxP = find(overlap > 0.6);
if length(idxP) > p_par.num_closest
    [~,sIdx] = sort(overlap(idxP),'descend');    
    idxP = idxP(sIdx(1:p_par.num_closest));
end
bbP  = ltt.grid(:,idxP);
if isempty(bbP), return; end

% Get hull
bbH  = bb_hull(bbP);
cols = bbH(1):bbH(3);
rows = bbH(2):bbH(4);

im1 = im0;
pEx = lttGetPattern(im1,bbP0,ltt.model.patchsize);
if ltt.model.fliplr
    pEx = [pEx lttGetPattern(im1,bbP0,ltt.model.patchsize,1)];
end
for i = 1:p_par.num_warps
    if i > 1
        randomize = rand; % Sets the internal randomizer to the same state
        %patch_input = img_patch(im0.input,bbH,randomize,p_par);
        patch_blur = img_patch(im0.blur,bbH,randomize,p_par);
        im1.blur(rows,cols) = patch_blur;
        %im1.input(rows,cols) = patch_input;
    end
    
    % Measures on blured image
    pX  = [pX fern(5,im1,idxP,0)];
    
    % Measures on input image
    %pEx(:,i) = lttGetPattern(im1,bbP0,ltt.model.patchsize);
    %pEx = [pEx lttGetPattern(im1,ltt.grid(1:4,idxP),ltt.model.patchsize)];
    
end