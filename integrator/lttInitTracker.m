
function ltt = lttInitTracker(ltt)

% load parameters from ltt structure
% target location and size
pos = floor(ltt.pos);
target_sz = floor(ltt.target_sz * ltt.trackerpara.resize_factor);

% tracker parameter
padding = ltt.trackerpara.padding;         			
output_sigma_factor = ltt.trackerpara.output_sigma_factor; 
translation_model_max_area = ltt.trackerpara.translation_model_max_area;
nScales = ltt.trackerpara.number_of_scales;
nScalesInterp = ltt.trackerpara.number_of_interp_scales;
scale_step = ltt.trackerpara.scale_step;
scale_sigma_factor = ltt.trackerpara.scale_sigma_factor;
scale_model_factor = ltt.trackerpara.scale_model_factor;
scale_model_max_area = ltt.trackerpara.scale_model_max_area;
num_compressed_dim = ltt.trackerpara.num_compressed_dim;
featureRatio = ltt.trackerpara.featureRatio;

init_target_sz = target_sz;

if prod(init_target_sz) > translation_model_max_area
    ltt.currentScaleFactor = sqrt(prod(init_target_sz)) / translation_model_max_area;
else
    ltt.currentScaleFactor = 1.0;
end

% target size at the initial scale
ltt.base_target_sz = target_sz / ltt.currentScaleFactor;

% window size, taking padding into accout
ltt.window_sz = floor(ltt.base_target_sz * (1 + padding));

output_sigma = sqrt(prod(floor(ltt.base_target_sz/featureRatio))) * output_sigma_factor;
use_sz = floor(ltt.window_sz/featureRatio);
rg = circshift(-floor((use_sz(1)-1)/2):ceil((use_sz(1)-1)/2), [0 -floor((use_sz(1)-1)/2)]);
cg = circshift(-floor((use_sz(2)-1)/2):ceil((use_sz(2)-1)/2), [0 -floor((use_sz(2)-1)/2)]);

[rs, cs] = ndgrid( rg,cg);
ltt.y = exp(-0.5 * (((rs.^2 + cs.^2) / output_sigma^2)));
ltt.yf = single(fft2(ltt.y));

ltt.interp_sz = size(ltt.y) * featureRatio;

ltt.cos_window = single(hann(floor(ltt.window_sz(1)/featureRatio))*hann(floor(ltt.window_sz(2)/featureRatio))' );

if nScales > 0
    scale_sigma = nScalesInterp * scale_sigma_factor;
    
    scale_exp = (-floor((nScales-1)/2):ceil((nScales-1)/2)) * nScalesInterp/nScales;
    scale_exp_shift = circshift(scale_exp, [0 -floor((nScales-1)/2)]);
    
    interp_scale_exp = -floor((nScalesInterp-1)/2):ceil((nScalesInterp-1)/2);
    interp_scale_exp_shift = circshift(interp_scale_exp, [0 -floor((nScalesInterp-1)/2)]);
    
    ltt.scaleSizeFactors = scale_step .^ scale_exp;
    ltt.interpScaleFactors = scale_step .^ interp_scale_exp_shift;
    
    ys = exp(-0.5 * (scale_exp_shift.^2) /scale_sigma^2);
    ltt.ysf = single(fft(ys));
    ltt.scale_window = single(hann(size(ltt.ysf,2)))';
    
    % make sure the scale model is not to large, to save computation time
    if scale_model_factor^2 * prod(init_target_sz) > scale_model_max_area
        ltt.scale_model_factor = sqrt(scale_model_max_area/prod(init_target_sz));
    else
        ltt.scale_model_factor = scale_model_factor;
    end
    
    % set the scale model size
    ltt.scale_model_sz = floor(init_target_sz * ltt.scale_model_factor);
    
    % load image
    im = ltt.source.im.origin;
    
    % force reasonable scale changes
    ltt.min_scale_factor = scale_step ^ ceil(log(max(5 ./ ltt.window_sz)) / log(scale_step));
    ltt.max_scale_factor = scale_step ^ floor(log(min([size(im,1) size(im,2)] ./ ltt.base_target_sz)) / log(scale_step));
    
    ltt.max_scale_dim = strcmp(ltt.trackerpara.s_num_compressed_dim,'MAX');
    if ltt.max_scale_dim
        ltt.s_num_compressed_dim = length(ltt.scaleSizeFactors);
    else
        ltt.s_num_compressed_dim = ltt.trackerpara.s_num_compressed_dim;
    end
end

% initialize the projection matrix
ltt.projection_matrix = [];

% =========================================================================
% computer coefficients for the translation filter
% =========================================================================
[xl_npca, xl_pca] = get_subwindow(im, pos, ltt.window_sz, ltt.currentScaleFactor);

ltt.h_num_pca = xl_pca;
ltt.h_num_npca = xl_npca;

% set number of compressed dimensions to maximum if too many
num_compressed_dim = min(num_compressed_dim, size(xl_pca,2));

data_matrix = ltt.h_num_pca;

[pca_basis, ~, ~] = svd(data_matrix'*data_matrix);
ltt.projection_matrix = pca_basis(:,1:num_compressed_dim);

hf_proj = fft2(feature_projection(ltt.h_num_npca,ltt.h_num_pca,ltt.projection_matrix,ltt.cos_window));
ltt.hf_num = bsxfun(@times, ltt.yf, conj(hf_proj));

xlf = fft2(feature_projection(xl_npca, xl_pca, ltt.projection_matrix, ltt.cos_window));
new_hf_den = sum(xlf.*conj(xlf),3);

ltt.hf_den = new_hf_den;

% compute coefficients for the scale filter
if nScales > 0
    
    % creat a new feature projection matrix
    [xs_pca, xs_npca] = get_scale_subwindow(im, pos, ltt.base_target_sz, ltt.currentScaleFactor*ltt.scaleSizeFactors,ltt.scale_model_sz);
    
    ltt.s_num = xs_pca;
    
    bigY = ltt.s_num;
    bigY_den = xs_pca;
    
    if ltt.max_scale_dim
        [ltt.scale_basis, ~] = qr(bigY,0);
        [ltt.scale_basis_den, ~] = qr(bigY_den,0);
    else
        [U,~,~] = svd(bigY,'econ');
        ltt.scale_basis = U(:,1:ltt.s_num_compressed_dim);
    end
    ltt.scale_basis = ltt.scale_basis';
    
    % create the filter update coefficients
    sf_proj = fft(feature_projection_scale([],ltt.s_num,ltt.scale_basis,ltt.scale_window),[],2);
    ltt.sf_num = bsxfun(@times, ltt.ysf, conj(sf_proj));
    
    xs = feature_projection_scale(xs_npca,xs_pca,ltt.scale_basis_den',ltt.scale_window);
    xsf = fft(xs,[],2);
    new_sf_den = sum(xsf.*conj(xsf),1);
    
    ltt.sf_den = new_sf_den;
    
end

end


