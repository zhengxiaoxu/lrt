
function ltt = lttInit(opt,ltt)

ltt = opt;

% INITIALIZE DETECTOR =====================================================

ltt = lttInitDetector(ltt);


% Initialize tracker ==================================================

ltt = lttInitTracker(ltt);

end
