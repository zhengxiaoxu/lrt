
function [ltt,BB2,Conf] = lttTracking(ltt,frame)

% load image
im = ltt.img{frame}.origin;

old_pos = inf(size(ltt.pos));
pos = ltt.pos;
iter = 1;

% tracker parameters
refinement_iterations = ltt.trackerpara.refinement_iterations;
lambda = ltt.trackerpara.lambda;
featureRatio = ltt.trackerpara.featureRatio;
interpolate_response = ltt.trackerpara.interpolate_response;
nScales = ltt.trackerpara.number_of_scales;
nScalesInterp = ltt.trackerpara.number_of_interp_scales;

% =========================================================================
% Translation search
% =========================================================================

while iter <= refinement_iterations && any(old_pos ~= pos)
    [xt_npca,xt_pca] = get_subwindow(im,pos,ltt.window_sz,ltt.currentScaleFactor);
    xt = feature_projection(xt_npca,xt_pca,ltt.projection_matrix,ltt.cos_window);
    xtf = fft2(xt);
    
    responsef = sum(ltt.hf_num.*xtf,3)./(ltt.hf_den + lambda);
    
    % if we undersampled features, we want to interpolate the response so
    % it has the same size as the image patch
    if interpolate_response > 0
        if interpolate_response == 2
            % use dynamic interp size
            ltt.interp_sz = floor(size(ltt.y) * featureRatio * ltt.currentScaleFactor);
        end
        responsef = resizeDFT2(responsef,ltt.interp_sz);
    end
    response = ifft2(responsef,'symmetric');
    
    [row, col] = find(response == max(response(:)),1);
    disp_row = mod(row - 1 + floor((ltt.interp_sz(1)-1)/2), ltt.interp_sz(1)) - floor((ltt.interp_sz(1)-1)/2);
    disp_col = mod(col - 1 + floor((ltt.interp_sz(2)-1)/2), ltt.interp_sz(2)) - floor((ltt.interp_sz(2)-1)/2);
            
    switch interpolate_response
        case 0
            translation_vec = round([disp_row, disp_col] * featureRatio * ltt.currentScaleFactor);
        case 1
            translation_vec = round([disp_row, disp_col] * ltt.currentScaleFactor);
        case 2
            translation_vec = [disp_row, disp_col];            
    end
    old_pos = pos;
    pos = pos + translation_vec;
    
    iter = iter + 1;  
end

% =========================================================================
% Scale search
% =========================================================================
if nScales > 0

    %create a new feature projection matrix
    [xs_pca, xs_npca] = get_scale_subwindow(im,pos,ltt.base_target_sz,ltt.currentScaleFactor*ltt.scaleSizeFactors,ltt.scale_model_sz);

    xs = feature_projection_scale(xs_npca,xs_pca,ltt.scale_basis,ltt.scale_window);
    xsf = fft(xs,[],2);

    scale_responsef = sum(ltt.sf_num .* xsf, 1) ./ (ltt.sf_den + lambda);

    interp_scale_response = ifft( resizeDFT(scale_responsef, nScalesInterp), 'symmetric');

    recovered_scale_index = find(interp_scale_response == max(interp_scale_response(:)), 1);

    % set the scale
    ltt.currentScaleFactor = ltt.currentScaleFactor * ltt.interpScaleFactors(recovered_scale_index);
    % adjust to make sure we are not to large or to small
    if ltt.currentScaleFactor < ltt.min_scale_factor
        ltt.currentScaleFactor = ltt.min_scale_factor;
    elseif ltt.currentScaleFactor > ltt.max_scale_factor
        ltt.currentScaleFactor = ltt.max_scale_factor;
    end
end

target_sz = floor(ltt.base_target_sz * ltt.currentScaleFactor);
ltt.pos = pos;
ltt.target_sz = target_sz;

BB2 = floor([pos(2)-target_sz(2)/2; pos(1)-target_sz(1)/2; pos(2)+target_sz(2)/2; pos(1)+target_sz(1)/2]);
Conf = max(response(:));

end

