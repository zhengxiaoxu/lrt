
function ltt = lttInitDetector(ltt)

[ltt.grid,ltt.scales] = bb_scan(ltt.source.bb,size(ltt.source.im.input),ltt.model.min_win,ltt.detectorpara.nScales,ltt.detectorpara.scale_step);

% Features
ltt.nGrid     = size(ltt.grid,2);
ltt.features  = lttGenerateFeatures(ltt.model.num_trees,ltt.model.num_features,0);

% Initialize Detector
fern(0); % cleanup
fern(1,ltt.source.im.input,ltt.grid,ltt.features,ltt.scales); % allocate structures

% Temporal structures
ltt.tmp.conf = zeros(1,ltt.nGrid);
ltt.tmp.patt = zeros(ltt.model.num_trees,ltt.nGrid);

% RESULTS =================================================================

% Initialize Trajectory
ltt.img     = cell(1,length(ltt.source.idx));
ltt.bb     = nan(4,length(ltt.source.idx));
ltt.conf    = nan(1,length(ltt.source.idx));
ltt.valid   = nan(1,length(ltt.source.idx));

% Fill first fields
ltt.img{1} = ltt.source.im;
ltt.bb(:,1) = ltt.source.bb;
ltt.conf(1) = 1;
ltt.valid(1)= 2;

ltt.box = ltt.source.bb;





ltt.source.imgsize = size(ltt.source.im.input);
ltt.X       = cell(1,length(ltt.source.idx)); % training data for fern
ltt.Y       = cell(1,length(ltt.source.idx)); 
ltt.pEx     = cell(1,length(ltt.source.idx)); % training data for NN
ltt.nEx     = cell(1,length(ltt.source.idx));
overlap     = bb_overlap(ltt.source.bb,ltt.grid); % bottleneck

% Target (display only)
ltt.target = img_patch(ltt.source.im.input,ltt.bb(:,1));

% Generate Positive Examples
[pX,pEx,bbP] = lttGeneratePositiveData(ltt,overlap,ltt.source.im,ltt.p_par_init);
pY = ones(1,size(pX,2));
% disp(['# P patterns: ' num2str(size(pX,2))]);
% disp(['# P patches : ' num2str(size(pEx,2))]);

% Correct initial bbox
% ltt.bb(:,1) = bbP(1:4,:);

% Variance threshold
ltt.var = var(pEx(:,1))/2;
% disp(['Variance : ' num2str(ltt.var)]);

% Generate Negative Examples
[nX,nEx] = lttGenerateNegativeData(ltt,overlap,ltt.source.im);
% disp(['# N patterns: ' num2str(size(nX,2))]);
% disp(['# N patches : ' num2str(size(nEx,2))]);

% Split Negative Data to Training set and Validation set
[nX1,nX2,nEx1,nEx2] = lttSplitNegativeData(nX,nEx);
nY1  = zeros(1,size(nX1,2));



ltt.pEx{1}  = pEx; % save positive patches for later
ltt.nEx{1}  = nEx; % save negative patches for later
ltt.X{1}    = [pX nX1];
ltt.Y{1}    = [pY nY1];
idx         = randperm(size(ltt.X{1},2));
ltt.X{1}    = ltt.X{1}(:,idx);
ltt.Y{1}    = ltt.Y{1}(:,idx);

% Train using training set ------------------------------------------------

% Fern
bootstrap = 2;
fern(2,ltt.X{1},ltt.Y{1},ltt.model.thr_fern,bootstrap);

% Nearest Neightbour 

ltt.pex = [];
ltt.nex = [];

ltt = lttTrainNN(pEx,nEx1,ltt);
ltt.model.num_init = size(ltt.pex,2);

% Estimate thresholds on validation set  ----------------------------------

% Fern
conf_fern = fern(3,nX2);
ltt.model.thr_fern = max(max(conf_fern)/ltt.model.num_trees,ltt.model.thr_fern);

% Nearest neighbor

conf_nn = lttNN(nEx2,ltt);
ltt.model.thr_nn = max(ltt.model.thr_nn,max(conf_nn));
% ltt.model.thr_nn_valid = max(ltt.model.thr_nn_valid,ltt.model.thr_nn);


