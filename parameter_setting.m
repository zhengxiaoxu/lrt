function opt = parameter_setting

% parameters for tracker -------------------------------------------------

padding = 2.0;                          % extra area surrounding the target
output_sigma_factor = 1/16;             % standard deviation for the desired translation filter output
scale_sigma_factor = 1/16;              % standard deviation for the desired scale filter output
lambda = 1e-2;                          % regularization weight (denote 'lambda' in the paper)
interp_factor = 0.025;                  % tracking model learning rate (denote 'eta' in the paper)
num_compressed_dim = 18;                % the dimensionality of the compressed features
refinement_iterations = 1;              % number of iterations used to refine the resulting position in a frame
translation_model_max_area = inf;       % maxium area of the translation model
interpolate_response = 1;               % interpolation method for the translation scores
resize_factor = 1;                      % initial resize
featureRatio = 4;

number_of_scales = 17;                     % number of scale levels
number_of_interp_scales = 33;           % number of scale levels after interpolation
scale_model_factor = 1.0;               % relative size of the scale sample
scale_step = 1.02;                      % scale increment factor (denote 'a' in the paper)
scale_model_max_area = 512;             % the maxium size of scal examples
s_num_compressed_dim = 'MAX';           % number of compressed scale fature dimensions

% parameters for detector -------------------------------------------------

num_trees = 10;
num_features = 13;

thr_fern = 0.6;
thr_nn = 0.65;

nScales_detector = 11;
scale_step_detector = 1.2;

% parameters for integrator------------------------------------------------

update_detector = 1;

thr_tracker = 0.05;

thr_detector = 0.5;

thr_update = 0.15;

thr_switch = 0.25;


% Algorithm Setting -------------------------------------------------------

opt.model           = struct('min_win',10,'patchsize',[15 15],'fliplr',0,'ncc_thesame',0.95,'valid',0.5,'num_trees',num_trees,'num_features',num_features,'thr_fern',thr_fern,'thr_nn',thr_nn);
opt.p_par_init      = struct('num_closest',10,'num_warps',20,'noise',5,'angle',20,'shift',0.02,'scale',0.02); % synthesis of positive examples during initialization
opt.p_par_update    = struct('num_closest',10,'num_warps',10,'noise',5,'angle',10,'shift',0.02,'scale',0.02); % synthesis of positive examples during update
opt.n_par           = struct('overlap_min',0.02,'overlap_max',0.2,'num_patterns',1000,'num_patches',20); % negative examples initialization/update
opt.control         = struct('maxbbox',1,'update_detector',update_detector,'drop_img',1,'repeat',1,'fps',50,'xsize_show',640,'xsize_process',320);
opt.plot            = struct('confidence',1,'save',0); 

opt.trackerpara        = struct('padding',padding,'output_sigma_factor',output_sigma_factor,'scale_sigma_factor',scale_sigma_factor,'lambda',lambda,...
    'interp_factor',interp_factor,'num_compressed_dim',num_compressed_dim,'refinement_iterations',refinement_iterations,'featureRatio',featureRatio,...
    'translation_model_max_area',translation_model_max_area,'interpolate_response',interpolate_response,'resize_factor',resize_factor,...
    'number_of_scales',number_of_scales,'number_of_interp_scales',number_of_interp_scales,'scale_model_factor',scale_model_factor,...
    'scale_step',scale_step,'scale_model_max_area',scale_model_max_area,'s_num_compressed_dim',s_num_compressed_dim);

opt.detectorpara    = struct('nScales',nScales_detector,'scale_step',scale_step_detector);

opt.switchpara      = struct('thr_tracker',thr_tracker,'thr_detector',thr_detector,'thr_switch',thr_switch,'thr_update',thr_update);


