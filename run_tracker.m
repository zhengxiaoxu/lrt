
function [distance_precision, PASCAL_precision, average_center_location_error, fps] =...
    run_tracker(video, show_visualization)

% default settings
if nargin < 1, video = 'choose'; end
if nargin < 2, show_visualization = ~strcmp(video, 'all'); end

% path to the videos
base_path = './data';

% load parameters
opt = parameter_setting;

% Running
switch video
    
    case 'choose'
        % ask the user for selecting the video, then call self with that video name.
        video = choose_video(base_path);
        if ~isempty(video)
            % start tracking
            [distance_precision, PASCAL_precision, average_center_location_error, fps] = run_tracker(video, show_visualization);
        end
        
    case 'all'
        % all videos, call self with each video name
        % only keep valid directory names
        dirs = dir(base_path);
        videos = {dirs.name};
        videos(strcmp('.', videos) | strcmp('..', videos) | ...
            strcmp('anno', videos) | ~[dirs.isdir]) = [];
        % Note: the 'Jogging' sequence has 2 tragets, create one entry for
        % each
        
%         videos(strcmpi('Jogging', videos)) = [];
%         videos(end+1:end+2) = {'Jogging.1', 'Jogging.2'};
        
        videos(strcmpi('Skating2', videos))=[];
        videos(end+1:end+2)={'Skating2.1', 'Skating2.2'};
        
        
        all_distance_precisions = zeros(numel(videos),1); % to compute averages
        all_PASCAL_precisions = zeros(numel(videos),1);
        all_average_center_location_errors = zeros(numel(videos),1);
        all_fps = zeros(numel(videos),1);        
        
        for k = 1:numel(videos)
            [all_distance_precisions(k), all_PASCAL_precisions(k),...
                all_average_center_location_errors(k), all_fps(k)] = run_tracker(videos{k},...
                show_visualization);
        end
                
        % compute average precision at 20px and FPS
        distance_precision = mean(all_distance_precisions);
        PASCAL_precision = mean(all_PASCAL_precisions);
        average_center_location_error = mean(all_average_center_location_errors);
        fps = mean(all_fps);
        
        % write the results
        fid = fopen('Result.txt','a+');
        fprintf(fid,'**********************************************************\n');
        fprintf(fid,'**********************************************************\n');
        fprintf(fid,'\nAverage distance precision (20px):% 1.3f\n', distance_precision);
        fprintf(fid,'\nAverage PASCAL precision (0.5):% 1.3f\n', PASCAL_precision);
        fprintf(fid,'\nAverage average_center_location_error:% 1.3f\n', average_center_location_error);
        fprintf(fid,'\nAverage FPS:% 4.2f\n\n', fps);
        fprintf(fid,'**********************************************************\n');
        fprintf(fid,'**********************************************************\n');
        fclose(fid);
        
    otherwise
        % given the name of a single video to process
        % get image file names, initial state, and ground truth for
        % evaluation
        [img_files, opt.pos, opt.target_sz, ground_truth, video_path] = ...
            load_video_info(base_path,video);

        opt.source.idx = 1:length(img_files);
        opt.source.bb = floor([opt.pos(2) - opt.target_sz(2)/2,...
            opt.pos(1) - opt.target_sz(1)/2, opt.pos(2) + opt.target_sz(2)/2,...
            opt.pos(1) + opt.target_sz(1)/2]'); % top_left and bottom_right
        disp('**********************************************************');
        fprintf('Processing video %12s...\n',video);
        
        % call the tracking and detection
        [positions,target_sz,time,num_detect] = lttExample(opt,video_path,img_files,show_visualization);
%         save positions.mat positions
%         save target_sz.mat target_sz
        
        % calculate the performance
        [distance_precision, PASCAL_precision, average_center_location_error] = ...
    compute_performance_measures([positions,target_sz], ground_truth, 20, 0.5);
        
       
        fps = numel(img_files) / time;
        
        % write the result
        fid = fopen('Result.txt','a+');
        fprintf(fid,'**********************************************************\n');
        fprintf(fid,'%12s - Distance Precision (20px):% 1.3f\n', video, distance_precision);
        fprintf(fid,'%12s - Overlap Precision (0.5):% 1.3f\n', video, PASCAL_precision);
        fprintf(fid,'%12s - Center Location Error:% 1.3f\n', video, average_center_location_error);
        fprintf(fid,'%12s - FPS:% 4.2f\n', video, fps);
        fprintf(fid,'%12s - number of successful detection:% 1.3f\n', video, num_detect);
        fclose(fid);
        
        % print the result
        fprintf('**********************************************************\n');
        fprintf('%12s - Distance Precision (20px):% 1.3f\n', video, distance_precision);
        fprintf('%12s - Overlap Precision (0.5):% 1.3f\n', video, PASCAL_precision);
        fprintf('%12s - Center Location Error:% 1.3f\n', video, average_center_location_error);
        fprintf('%12s - FPS:% 4.2f\n', video, fps);
        fprintf('%12s - number of successful detection:% 1.3f\n', video, num_detect);
end











