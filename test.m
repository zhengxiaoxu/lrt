%% ----------------------------------------------------------------------
%  This is the demo code to run the long-term tracking 
%  ----------------------------------------------------------------------

clear all;
close all;
clc;

addpath(genpath('utilities'));
addpath(genpath('tracker'));
addpath(genpath('detector'));
addpath(genpath('integrator'));

[distance_precision, PASCAL_precision, average_center_location_error, fps] = run_tracker('choose',1);



